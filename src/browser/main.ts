import {
  BrowserMessageReader,
  BrowserMessageWriter,
  createConnection,
} from 'vscode-languageserver/browser';

const messageReader = new BrowserMessageReader(self);
const messageWriter = new BrowserMessageWriter(self);

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const connection = createConnection(messageReader, messageWriter);

// TODO: ...
