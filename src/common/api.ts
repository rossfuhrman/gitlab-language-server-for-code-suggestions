import fetch from 'cross-fetch';

import { IClientInfo } from './config';
import { IDocContext } from './code-suggestions';

export interface IGitLabAPI {
  getCodeSuggestions(file_info: IDocContext): Promise<CodeSuggestionResponse | undefined>;
  setToken(token: string): void;
  setClientInfo(clientInfo: IClientInfo | undefined): void;
}

export interface CodeSuggestionRequest {
  prompt_version: number;
  project_path: string;
  project_id: number;
  current_file: CodeSuggestionRequestCurrentFile;
}

export interface CodeSuggestionRequestCurrentFile {
  file_name: string;
  content_above_cursor: string;
  content_below_cursor: string;
}

export interface CodeSuggestionResponse {
  choices: CodeSuggestionResponseChoice[] | undefined;
}

export interface CodeSuggestionResponseChoice {
  text: string;
}

export interface CompletionToken {
  access_token: string;
  /* expires in number of seconds since `created_at` */
  expires_in: number;
  /* unix timestamp of the datetime of token creation */
  created_at: number;
}

export class GitLabAPI implements IGitLabAPI {
  #completionToken: CompletionToken | undefined;
  #token: string | undefined;
  #baseURL: string;
  #clientInfo: IClientInfo | undefined;

  constructor(baseURL = 'https://gitlab.com', token?: string) {
    this.#token = token;
    this.#baseURL = baseURL;
  }

  setToken(token?: string) {
    this.#token = token;
    this.#completionToken = undefined;
  }

  async getCodeSuggestions(context: IDocContext): Promise<CodeSuggestionResponse | undefined> {
    await this.#getCompletionToken();

    const request: CodeSuggestionRequest = {
      prompt_version: 1,
      project_path: '',
      project_id: -1,
      current_file: {
        content_above_cursor: context.prefix,
        content_below_cursor: context.suffix,
        file_name: context.filename,
      },
    };

    const headers = {
      Authorization: `Bearer ${this.#completionToken?.access_token}`,
      'Content-Type': 'application/json',
      'X-Gitlab-Authentication-Type': 'oidc',
      'User-Agent': `code-completions-language-server-experiment (${this.#clientInfo?.name}:${this
        .#clientInfo?.version})`,
    };

    const response = await fetch(`https://codesuggestions.gitlab.com/v2/completions`, {
      method: 'POST',
      headers,
      body: JSON.stringify(request),
    } as RequestInit);

    return response.json();
  }

  setClientInfo(clientInfo: IClientInfo) {
    this.#clientInfo = clientInfo;
  }

  async #getCompletionToken(): Promise<string> {
    // Check if token has expired
    if (this.#completionToken) {
      const unixTimestampNow = Math.floor(new Date().getTime() / 1000);
      if (unixTimestampNow < this.#completionToken.created_at + this.#completionToken.expires_in) {
        return this.#completionToken.access_token;
      }
    }

    if (!this.#token) {
      throw 'Token needs to be provided';
    }

    const headers = {
      Authorization: `Bearer ${this.#token}`,
      'User-Agent': `code-completions-language-server-experiment (${this.#clientInfo?.name}:${this
        .#clientInfo?.version})`,
    };

    const response = await fetch(`${this.#baseURL}/api/v4/code_suggestions/tokens`, {
      method: 'POST',
      headers,
    } as RequestInit);

    this.#completionToken = (await response.json()) as CompletionToken;
    return this.#completionToken.access_token;
  }
}
