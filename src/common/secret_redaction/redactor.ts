import { convertToJavaScriptRegex } from './helpers';
import rules, { IGitleaksRule } from './gitleaks_rules';

export class SecretRedactor {
  #rules: IGitleaksRule[] = [];

  constructor() {
    this.#parseRules();
  }

  redactSecrets(raw: string): string {
    return this.#rules.reduce((redacted: string, rule: IGitleaksRule) => {
      return this.#redactRuleSecret(redacted, rule);
    }, raw);
  }

  #redactRuleSecret(str: string, rule: IGitleaksRule): string {
    if (!rule.compiledRegex) return str;

    if (!rule.compiledRegex.test(str)) {
      return str;
    }

    const secrets = str.match(rule.compiledRegex) || [];
    let redacted = str;

    for (const secret of secrets) {
      const mask = '*'.repeat(secret.length);
      redacted = redacted.replace(new RegExp(secret, 'g'), mask);
    }

    return redacted;
  }

  #parseRules() {
    try {
      this.#rules = rules.reduce((rules: Array<IGitleaksRule>, rule: IGitleaksRule) => {
        let compiledRegex;

        try {
          /*  TODO: the helper that converts the PCRE regex to the JS flavor is very basic
            we should proceed looking for better ways to transform the regex or add more tests */
          const jsRegex = convertToJavaScriptRegex(rule.regex);
          compiledRegex = new RegExp(jsRegex);
          rules.push({
            ...rule,
            compiledRegex,
          });
        } catch (error) {
          console.log(`Failed to compile "${rule.description}" regex`);
        }
        return rules;
      }, []);
    } catch (error) {
      console.error(`Failed to parse GitLeaks config file: ${error}`);
    }
  }
}
