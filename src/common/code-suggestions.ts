import { Position, TextDocument } from 'vscode-languageserver-textdocument';
import { SecretRedactor } from './secret_redaction/redactor';

export interface IDocContext {
  prefix: string;
  suffix: string;
  filename: string;
}

export function getDocContext(
  document: TextDocument,
  position: Position,
  redactor: SecretRedactor | undefined,
): IDocContext {
  let prefix = document.getText({ start: document.positionAt(0), end: position });

  let suffix = document.getText({
    start: position,
    end: document.positionAt(document.getText().length),
  });

  if (redactor) {
    [prefix, suffix] = [prefix, suffix].map(redactor.redactSecrets.bind(redactor));
  }

  return {
    prefix,
    suffix,
    filename: document.uri.slice(document.uri.lastIndexOf('/') + 1),
  };
}
