import { InitializeParams } from 'vscode-languageserver';

export type IClientInfo = InitializeParams['clientInfo'];
interface ICodeCompletionConfig {
  enableSecretRedaction: boolean;
}

export interface IConfig {
  gitlabUrl?: string;
  token?: string;
  clientInfo?: IClientInfo;
  codeCompletion?: ICodeCompletionConfig;
}
