import { SecretRedactor } from '../../../common/secret_redaction/redactor';

const textWithSecrets = `File with various fake secrets to test redaction with Gitleaks rules.

aws-key=AKIALALEMEL33243OKIA
pypiToken: pypi-AgEIcHlwaS5vcmcAAAAAAAAAA-AAAAAAAAAA-AAAAAAAAAA-AAAAAAAAAA-AAAAAAAAAA-AAAAAAAAAAB
glpat="glpat-deadbeefdeadbeefdead"
`;
const redactedText = `File with various fake secrets to test redaction with Gitleaks rules.

aws-key=********************
pypiToken: **************************************************************************************
glpat="**************************"
`;

describe('Secrets Redaction', () => {
  let redactor: SecretRedactor;

  beforeEach(async () => {
    redactor = new SecretRedactor();
  });

  describe('redactSecrets', () => {
    it('should redact the secrets from the string', () => {
      expect(redactor.redactSecrets(textWithSecrets)).toBe(redactedText);
    });
  });
});
