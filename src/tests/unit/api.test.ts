import fetch from 'cross-fetch';
import { GitLabAPI, CodeSuggestionResponse } from '../../common/api';
import { CODE_SUGGESTIONS_RESPONSE, CODES_SUGGESTIONS_COMPLETION_TOKEN, FILE_INFO } from './mocks';

jest.mock('cross-fetch');

describe('GitLabAPI', () => {
  let gitLabAPI: GitLabAPI;
  const token = 'token';
  const baseUrl = 'http://test.com';
  const clientInfo = { name: 'MyClient', version: '1.0.0' };

  describe('getCodeSuggestions', () => {
    describe('Error path', () => {
      beforeEach(() => {
        gitLabAPI = new GitLabAPI(baseUrl, undefined);
        gitLabAPI.setClientInfo({ name: clientInfo.name, version: clientInfo.version });
      });

      it('should throw an error when no token provided', async () => {
        try {
          await gitLabAPI.getCodeSuggestions(FILE_INFO);
        } catch (error) {
          expect(error).toBe('Token needs to be provided');
        }
      });
    });

    describe('Success path', () => {
      let response: CodeSuggestionResponse | undefined;

      beforeEach(async () => {
        gitLabAPI = new GitLabAPI(baseUrl, token);
        gitLabAPI.setClientInfo({ name: clientInfo.name, version: clientInfo.version });

        (fetch as jest.Mock)
          .mockResolvedValueOnce({
            json: () => CODES_SUGGESTIONS_COMPLETION_TOKEN,
          })
          .mockResolvedValueOnce({
            json: () => CODE_SUGGESTIONS_RESPONSE,
          });

        response = await gitLabAPI.getCodeSuggestions(FILE_INFO);
      });

      it('should make a request for the access token', () => {
        const [url, params] = (fetch as jest.Mock).mock.calls[0];

        expect(url).toBe(`${baseUrl}/api/v4/code_suggestions/tokens`);

        expect(params.headers).toMatchObject({
          Authorization: `Bearer ${token}`,
          'User-Agent': `code-completions-language-server-experiment (${clientInfo.name}:${clientInfo.version})`,
        });
      });

      it('should make a request for the code suggestions', () => {
        const [url, params] = (fetch as jest.Mock).mock.calls[1];

        expect(url).toBe('https://codesuggestions.gitlab.com/v2/completions');

        expect(params.headers).toMatchObject({
          Authorization: `Bearer ${CODES_SUGGESTIONS_COMPLETION_TOKEN.access_token}`,
          'Content-Type': 'application/json',
          'X-Gitlab-Authentication-Type': 'oidc',
          'User-Agent': `code-completions-language-server-experiment (${clientInfo.name}:${clientInfo?.version})`,
        });
      });

      it('should return code suggestions', async () => {
        expect(response).toEqual(CODE_SUGGESTIONS_RESPONSE);
      });
    });
  });
});
