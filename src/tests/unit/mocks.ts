import { CodeSuggestionResponse, CompletionToken } from '../../common/api';
import { IDocContext } from '../../common/code-suggestions';

export const FILE_INFO: IDocContext = {
  filename: 'example.ts',
  prefix: 'const x = 10;',
  suffix: 'console.log(x);',
};

export const CODE_SUGGESTIONS_RESPONSE: CodeSuggestionResponse = {
  choices: [{ text: 'choice1' }, { text: 'choice2' }],
};

export const CODES_SUGGESTIONS_COMPLETION_TOKEN: CompletionToken = {
  access_token: 'competion_token',
  expires_in: 3600,
  created_at: 1592227302,
};
