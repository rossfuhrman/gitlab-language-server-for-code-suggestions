import {
  CompletionItem,
  CompletionItemKind,
  DidChangeConfigurationNotification,
  InitializeParams,
  TextDocumentPositionParams,
  TextDocuments,
  createConnection,
} from 'vscode-languageserver/node';
import { TextDocument } from 'vscode-languageserver-textdocument';
import { SecretRedactor } from '../common/secret_redaction/redactor';

import { IConfig } from '../common/config';
import { getDocContext } from '../common/code-suggestions';
import { GitLabAPI } from '../common/api';

const connection = createConnection();
const documents: TextDocuments<TextDocument> = new TextDocuments(TextDocument);
const config: IConfig = {};

const api = new GitLabAPI();
let redactor: SecretRedactor;

connection.onInitialize(
  ({ clientInfo, initializationOptions: { codeCompletion } }: InitializeParams) => {
    Object.assign(config, { clientInfo, codeCompletion });

    return {
      capabilities: {
        completionProvider: {
          resolveProvider: true,
        },
      },
    };
  },
);

connection.onInitialized(async () => {
  // eslint-disable-next-line @typescript-eslint/no-floating-promises
  connection.client.register(DidChangeConfigurationNotification.type);

  // if client provided the config to redact secrets
  // create the redactor and load the rules to be used in the `onCompletion` handler
  if (config.codeCompletion?.enableSecretRedaction) {
    redactor = new SecretRedactor();
  }
});

connection.onDidChangeConfiguration((change) => {
  Object.assign(config, change.settings || { token: null });
  api.setToken(config.token);
});

// This handler provides the initial list of the completion items.
connection.onCompletion(
  async ({ textDocument, position }: TextDocumentPositionParams): Promise<CompletionItem[]> => {
    try {
      const doc = documents.get(textDocument.uri);

      if (doc === undefined) {
        return [];
      }

      const fileInfo = getDocContext(doc, position, redactor);
      const suggestions = await api.getCodeSuggestions(fileInfo);

      if (suggestions?.choices === undefined) {
        return [];
      }

      return suggestions.choices.map((choice, index) => ({
        label: `GitLab Suggestion ${index + 1}`,
        kind: CompletionItemKind.Text,
        insertText: choice.text,
        data: index,
      }));
    } catch (e) {
      return [];
    }
  },
);

connection.onCompletionResolve((item: CompletionItem) => item);

// Make the text document manager listen on the connection
// for open, change and close text document events
documents.listen(connection);

// Listen on the connection
connection.listen();
