# Change Log

All notable changes to this project will be documented in this file.

## v0.0.14

- Re-arrange deploy, and remove if logic

## v0.0.13

- Increment

## v0.0.12

- Variables

## v0.0.11

- Back to deploy.sh

## v0.0.10

- Keep progressing

## v0.0.9

- Different approach

## v0.0.8

- NPM publishing setup attempt

## v0.0.7

- Use the correct remote

## v0.0.6

- Need to set email address in git config

## v0.0.5

- Now creating tags

## v0.0.4

- A bit more automated

## v0.0.3

- More testing

## v0.0.2

- Creating a new tag

## v0.0.1

- Base version
