#!/usr/bin/env bash

## Release binaries in the package registry and attach assets to release
SEMVER_PAT="[0-9][0-9]*\.[0-9][0-9]*\.[0-9][0-9]*"
LATEST=$(egrep -o "## v${SEMVER_PAT}" CHANGELOG.md | tr -d ' #' | head -n 1)
#TAG_PREFIX=v

[ "$LATEST" == "" ] && {
	echo "unable to extract latest version"
	exit 1
}

find ./bin -type f -name "gitlab-lsp-*" | while read -r binfile; do
	BASE=$(basename ${binfile})
    URL="https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/packages/generic/gitlab-language-server/${LATEST}/${BASE}"

    echo $BASE
    echo $URL
	curl --fail --header "JOB-TOKEN: $CI_JOB_TOKEN" "$URL" -o /dev/null --silent && {
		echo "${URL} already present"
		continue
	}

	echo "uploading '${binfile}' to '${URL}'"
	curl --fail --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file "$binfile" "$URL" || {
		echo "unable to upload ${URL}"
		exit 1
	}

	## attach released binary to (already existing) release
	curl --request POST \
	    --header "JOB-TOKEN: $CI_JOB_TOKEN" \
	    --data name="$BASE" \
	    --data url="$URL" \
	    "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/gitlab-language-server/${LATEST}/assets/links"
done

# BOT_USER and GITLAB_TOKEN are set in CI/CD variables inside the project
## Tag build ########################################
#git tag -a ${TAG_PREFIX}${LATEST} -m '[skip ci]'
git config --global user.email "${BOT_USER}@gitlab.com"
git config --global user.name "GitLab LSP bot"
git tag -a ${LATEST} -m '[skip ci]'
# push with the bot user
#git push --tags -o ci.skip --repo=https://$BOT_USER:$GITLAB_TOKEN@gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio-extension.git
git push --tags -o ci.skip --repo=https://$BOT_USER:$GITLAB_TOKEN@gitlab.com/rossfuhrman/gitlab-language-server-for-code-suggestions.git


## Publish the package. ########################################
# If the version in package.json has not yet been published, it will be
# published to GitLab's NPM registry. If the version already exists, the publish command
# will fail and the existing package will not be updated.
NPM_PACKAGE_NAME=$(node -p "require('./package.json').name")
NPM_PACKAGE_VERSION=$(node -p "require('./package.json').version")
echo "Attempting to publish version ${NPM_PACKAGE_VERSION} of ${NPM_PACKAGE_NAME} to GitLab's NPM registry: ${CI_PROJECT_URL}/-/packages"

npm publish

exit 0
